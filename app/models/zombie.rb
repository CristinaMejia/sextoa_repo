class Zombie < ApplicationRecord
    has_many :brains, dependent: :destroy
    validates :name, presence: true
    mount_uploader :avatar, AvatarUploader
    validates :bio, length: { maximum: 100 }
    validates :age, numericality:{only_integer:true, message: "Solo numeros enteros"}
    validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
    belongs_to :user
end
